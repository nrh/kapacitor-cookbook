kapacitor CHANGELOG
===================

This file is used to list changes made in each version of the kapacitor cookbook.

v0.1.0
-----
- Initial release of kapacitor cookbook, supports basic installation and config

