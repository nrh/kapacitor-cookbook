require 'spec_helper'

describe 'kapacitor' do
  describe user('kapacitor') do
    it { is_expected.to exist }
  end

  describe service('kapacitor') do
    it { is_expected.to be_running }
  end

  describe port(9092) do
    it { is_expected.to be_listening }
  end
end
