name 'kapacitor'
maintainer 'Michal Taborsky'
maintainer_email 'michal.taborsky@mall.cz'
license 'Apache v2.0'
description 'Installs/Configures Influxdata Kapacitor'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '0.1.0'
source_url 'https://gitlab.com/nrh/kapacitor-cookbook'

depends 'yum'
depends 'apt'

supports 'redhat'
supports 'centos'
supports 'debian'
supports 'ubuntu'
