property :name, String, name_property: true
property :install_version, String

default_action :create

action :create do
  if platform_family? 'rhel'
    yum_repository 'kapacitor' do
      description 'InfluxDB Repository - RHEL \$releasever'
      baseurl 'https://repos.influxdata.com/centos/\$releasever/\$basearch/stable'
      gpgkey 'https://repos.influxdata.com/influxdb.key'
    end
  else
    package 'apt-transport-https'

    apt_repository 'influxdb' do
      uri "https://repos.influxdata.com/#{node['platform']}"
      distribution node['lsb']['codename']
      components ['stable']
      arch 'amd64'
      key 'https://repos.influxdata.com/influxdb.key'
    end
  end

  package 'kapacitor' do
    version install_version
  end

  service "kapacitor_#{name}" do
    service_name 'kapacitor'
    action [:enable, :start]
  end
end

action :delete do
  service "kapacitor_#{name}" do
    service_name 'kapacitor'
    action [:stop, :disable]
  end

  package 'kapacitor' do
    action :remove
  end
end
