property :name, String, name_property: true
property :config, Hash, default: {}
property :path, String, default: node['kapacitor']['config_file_path']

default_action :create

action :create do
  chef_gem 'toml-rb' do
    compile_time true if respond_to?(:compile_time)
  end

  require 'toml'

  service "kapacitor_#{new_resource.name}" do
    service_name 'kapacitor'
    action :nothing
  end

  file path do
    content TOML.dump(config)
    notifies :restart, "service[kapacitor_#{new_resource.name}]", :delayed
  end
end
