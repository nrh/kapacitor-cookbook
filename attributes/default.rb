default['kapacitor']['config_file_path'] = '/etc/kapacitor/kapacitor.conf'
default['kapacitor']['version'] = '0.10.0-1'

default['kapacitor']['config'] = {
  'hostname' => node.fqdn,
  'data_dir' => '/var/lib/kapacitor',
  'http' => {
    'bind-address' => ':9092',
    'auth-enabled' => false,
    'log-enabled' => true,
    'write-tracing' => false,
    'pprof-enabled' => false,
    'https-enabled' => false,
    'https-certificate' => '/etc/ssl/kapacitor.pem'
  },
  'logging' => {
    'file' => '/var/log/kapacitor/kapacitor.log',
    'level' => 'INFO'
  },
  'replay' => {
    'dir' => '/var/lib/kapacitor/replay'
  },
  'task' => {
    'dir' => '/var/lib/kapacitor/tasks',
    'snapshot-interval' => '60s'
  }
}
