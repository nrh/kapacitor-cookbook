kapacitor_install 'default' do
  install_version node['kapacitor']['version']
  action :create
end

kapacitor_config 'default' do
  path node['kapacitor']['config_file_path']
  config node['kapacitor']['config']
end
