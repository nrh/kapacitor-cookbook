kapacitor Cookbook
==================

Installs and configures Kapacitor, part of the TICK stack from Influxdata 
https://influxdata.com/.

Usage
-----

To install Kapacitor, either include the `kapacitor::default` recipe
in the run list or use the provided resources in your wrapper cookbook.

Attributes
----------

  * `default['kapacitor']['config_file_path']` - used by the default recipe
    to determine where to put the configuration 
  * `default['kapacitor']['version']` - the version, including release, to install
  * `default['kapacitor']['config']` - the configuration file is generated from
    this hash 

Resources
---------

### kapacitor_install

Adds the Influxdata repository to the system and installs via packages
 
```ruby
kapacitor_install 'default' do
  install_version node['kapacitor']['version']
  action :create
end
```

### kapacitor_config

Creates the configuration file from `node['kapacitor']['config']`.

```ruby
kapacitor_config 'default' do
  path node['kapacitor']['config_file_path']
  config node['kapacitor']['config']
end
```

Testing
-------

The test suite includes:
- Rubocop
- Foodcritic
- Kitchen

To run the static analysis tests, use:

```
bundle exec rake test:quick
```

To run the full suite including Kitchen, use:

```
bundle exec rake test:complete
```

Contributing
------------

1. Fork the repository on Gitlab
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Merge Request using Gitlab

Credits
-------

This cookbook is inspired by https://github.com/bdangit/chef-influxdb and
https://github.com/influxdata/telegraf, with some bits shamelessly copy-pasted. 

License and Authors
-------------------

Copyright 2016 Internet Mall, a.s.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

Authors:
- Michal Taborsky <michal.taborsky@mall.cz>

